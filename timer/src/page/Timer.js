import React, {useEffect, useState} from 'react'

export const Timer = () => {
    const [time, setTime] = useState(0)
    const [timer, setTimer] = useState(false)
    const resettimer =() =>{
        setTimer(false)
        setTime(0)
    }
    
    useEffect(() => {
        let interval;
        if(timer){
            interval = setInterval(() => {
                setTime((prevTime) => prevTime + 1000);
            }, 1000);
        } else if(!timer){
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [timer]);

  return (
    <div className='App'>
        <div className='App-header'>
            <div className='rectangle App-header'>
                <span className='App-header'>{Math.floor((time/3600000 % 24))} : {Math.floor((time/60000 %60))} : {Math.floor((time/1000 %60))}</span>
                <span className='App-header'>Jam : Menit : Detik</span>
            </div>

            <div className='buttons'> 
                <button onClick={()=>{resettimer()}} className='oval yellow'>Reset</button> 
                <span className='spasi'></span>
                <button onClick={()=>{setTimer(true)}} className='oval green'>Start</button>
                <span className='spasi'></span>
                <button onClick={()=>{setTimer(false)}} className='oval oldgreen'>Stop</button>
            </div>
        </div>
    </div>
  )
}
