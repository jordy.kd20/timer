import React from 'react';
import ReactDOM from 'react-dom/client';
import './css/timer.css'
import { Timer } from './page/Timer';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Timer/>
  </React.StrictMode>
);


